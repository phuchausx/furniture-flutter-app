import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_furniture/helpers/asset_helper.dart';
import 'package:flutter_furniture/helpers/image_helper.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});
  static const String routeName = '/splash_screen';

  static Route route() {
    return MaterialPageRoute(
      settings: const RouteSettings(name: routeName),
      builder: (_) => const SplashScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    Timer(const Duration(seconds: 3), () => Navigator.pushNamed(context, '/'));
    return Scaffold(
      body: ImageHelper.loadFromAsset(
        AssetHelper.coverImage,
        fit: BoxFit.cover,
        width: double.infinity,
        height: double.infinity,
      ),
    );
  }
}
