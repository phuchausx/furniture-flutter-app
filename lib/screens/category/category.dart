import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_furniture/blocs/productsByCategory/products_by_category_bloc.dart';
import 'package:flutter_furniture/models/models.dart';
import 'package:flutter_furniture/widgets/widgets.dart';

class CategoryScreen extends StatelessWidget {
  const CategoryScreen({
    super.key,
    required this.category,
  });

  static const String routeName = '/category';

  static Route route({required Category category}) {
    return MaterialPageRoute(
      settings: const RouteSettings(name: routeName),
      builder: (context) => CategoryScreen(category: category),
    );
  }

  final Category category;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppBar(screen: routeName),
      body: Padding(
        padding: const EdgeInsets.only(top: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Text(
                category.nameCategory,
                style: const TextStyle(
                  fontSize: 22,
                  fontWeight: FontWeight.w900,
                ),
              ),
            ),
            const SizedBox(height: 20),
            TabCategory(
              idCategory: category.idCategory,
              categoryDetail: category.categoryDetails,
            ),
            BlocBuilder<ProductsByCategoryBloc, ProductsByCategoryState>(
              builder: (context, state) {
                if (state is ProductsByCategoryLoading) {
                  return const Center(child: CircularProgressIndicator());
                }
                if (state is ProductsByCategoryLoaded) {
                  return Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: GridView.builder(
                        itemCount: state.products.length,
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          mainAxisSpacing: 20,
                          crossAxisSpacing: 20,
                          childAspectRatio: 0.66,
                        ),
                        itemBuilder: (context, index) => ProductCard(
                          product: state.products[index],
                          widthFactor: 1,
                        ),
                      ),
                    ),
                  );
                } else {
                  return const Text('Something went wrong');
                }
              },
            )
          ],
        ),
      ),
    );
  }
}
