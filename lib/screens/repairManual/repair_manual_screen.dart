import 'package:flutter/material.dart';
import 'package:flutter_furniture/helpers/image_helper.dart';
import 'package:flutter_furniture/models/models.dart';
import 'package:flutter_furniture/widgets/widgets.dart';

class RepairManualScreen extends StatelessWidget {
  const RepairManualScreen({super.key, required this.repairManual});

  static const String routeName = '/repair_manual_screen';

  static Route route({required RepairManual repairManual}) {
    return MaterialPageRoute(
        settings: const RouteSettings(name: routeName),
        builder: (context) => RepairManualScreen(repairManual: repairManual));
  }

  final RepairManual repairManual;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppBar(screen: routeName),
      body: Padding(
        padding: const EdgeInsets.only(top: 20),
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: ImageHelper.loadFromAsset(
                repairManual.imagePath,
                fit: BoxFit.cover,
                width: double.infinity,
                height: double.infinity,
              ),
            ),
            Expanded(
              flex: 2,
              child: Container(
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      repairManual.titleRepairManual,
                      style: const TextStyle(
                        fontSize: 20,
                        color: Colors.black,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    Text(
                      '${repairManual.numberView.toString()} lượt xem',
                      style: const TextStyle(
                        fontSize: 14,
                        color: Colors.grey,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    const SizedBox(height: 30),
                    Text(
                      repairManual.content,
                      style: const TextStyle(
                        fontSize: 15,
                        color: Colors.black,
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
