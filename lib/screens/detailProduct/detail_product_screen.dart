import 'package:flutter/material.dart';
import 'package:flutter_furniture/helpers/image_helper.dart';
import 'package:flutter_furniture/models/models.dart';
import 'package:flutter_furniture/widgets/widgets.dart';
import 'package:intl/intl.dart';

class DetailProductScreen extends StatelessWidget {
  const DetailProductScreen({super.key, required this.product});

  static const String routeName = '/detail_product_screen';

  static Route route({required Product product}) {
    return MaterialPageRoute(
      settings: const RouteSettings(name: routeName),
      builder: (context) => DetailProductScreen(product: product),
    );
  }

  final Product product;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final format = NumberFormat("###,###.###", "tr_TR");
    return Scaffold(
      appBar: const CustomAppBar(screen: routeName),
      bottomNavigationBar: CustomNavBar(screen: routeName, product: product),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: size.height * 7 / 8,
              color: Colors.grey.shade400,
              child: Stack(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: size.height * 0.3),
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 70,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              RichText(
                                text: TextSpan(children: [
                                  const TextSpan(
                                    text: 'Nhãn hiệu\n',
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 18,
                                    ),
                                  ),
                                  const WidgetSpan(
                                    child: SizedBox(
                                      height: 25,
                                    ),
                                  ),
                                  TextSpan(
                                    text: product.brand,
                                    style: const TextStyle(
                                      color: Colors.black,
                                      fontSize: 20,
                                      fontWeight: FontWeight.w700,
                                    ),
                                  )
                                ]),
                              ),
                              RichText(
                                text: TextSpan(children: [
                                  const TextSpan(
                                    text: 'Kích thước\n',
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 18,
                                    ),
                                  ),
                                  const WidgetSpan(
                                    child: SizedBox(
                                      height: 25,
                                    ),
                                  ),
                                  TextSpan(
                                    text: product.size,
                                    style: const TextStyle(
                                      color: Colors.black,
                                      fontSize: 20,
                                      fontWeight: FontWeight.w700,
                                    ),
                                  )
                                ]),
                              ),
                            ],
                          ),
                          const SizedBox(height: 30),
                          Text(
                            product.description,
                            style: const TextStyle(
                              fontSize: 17,
                              color: Colors.black,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  ProductTitleWithImage(product: product, format: format)
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class ProductTitleWithImage extends StatelessWidget {
  const ProductTitleWithImage({
    super.key,
    required this.product,
    required this.format,
  });

  final Product product;
  final NumberFormat format;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 20),
          Text(
            product.nameCategory,
            style: const TextStyle(color: Colors.black, fontSize: 20),
          ),
          const SizedBox(height: 5),
          Text(
            product.name,
            style: const TextStyle(
              color: Colors.black,
              fontSize: 26,
              fontWeight: FontWeight.w700,
            ),
          ),
          const SizedBox(height: 16),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: const EdgeInsets.only(top: 25),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'GIÁ:',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 19,
                      ),
                    ),
                    const SizedBox(height: 10),
                    PriceProduct.loadProperty(
                      price: product.price,
                      discount: product.discount,
                      fontSizeDiscount: 15,
                      fontSizePrice: 13,
                    ),
                  ],
                ),
              ),
              const SizedBox(
                width: 20,
              ),
              Expanded(
                child: Hero(
                  tag: '${product.idProduct}',
                  child: ImageHelper.loadFromAsset(
                    product.imageUrl,
                    fit: BoxFit.contain,
                    height: 230,
                    alignment: Alignment.topRight,
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
