import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_furniture/blocs/listProductSearch/list_product_search_bloc.dart';
import 'package:flutter_furniture/widgets/widgets.dart';

class SearchResultScreen extends StatelessWidget {
  const SearchResultScreen({super.key});

  static const String routeName = '/search_detail_screen';

  static Route route() {
    return MaterialPageRoute(
      settings: const RouteSettings(name: routeName),
      builder: (context) => const SearchResultScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppBar(screen: routeName),
      body: BlocBuilder<ListProductSearchBloc, ListProductSearchState>(
        builder: (context, state) {
          if (state is ListProductSearchLoading) {
            return const Expanded(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }

          if (state is ListProductSearchLoaded) {
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: 20),
                  Text(
                    'Có ${state.products.length} kết quả phù hợp',
                    style: const TextStyle(fontSize: 17),
                  ),
                  const SizedBox(height: 20),
                  Expanded(
                    child: GridView.builder(
                      itemCount: state.products.length,
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        mainAxisSpacing: 20,
                        crossAxisSpacing: 20,
                        childAspectRatio: 0.66,
                      ),
                      itemBuilder: (context, index) => ProductCard(
                        product: state.products[index],
                        widthFactor: 1,
                      ),
                    ),
                  )
                ],
              ),
            );
          } else {
            return const Text('Something went wrong');
          }
        },
      ),
    );
  }
}
