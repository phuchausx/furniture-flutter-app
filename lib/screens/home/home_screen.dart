import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_furniture/models/models.dart';
import 'package:flutter_furniture/widgets/widgets.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  static const String routeName = '/';

  static Route route() {
    return MaterialPageRoute(
      settings: const RouteSettings(name: routeName),
      builder: (_) => const HomeScreen(),
    );
  }

  @override
  Widget build(BuildContext context) => ScaffoldMessenger(
        child: Builder(
          builder: (context) => Scaffold(
            appBar: const CustomAppBar(screen: routeName),
            body: SingleChildScrollView(
              child: Column(
                children: [
                  const SizedBox(
                    height: 8,
                  ),
                  // event
                  CarouselSlider(
                    options: CarouselOptions(
                      autoPlay: true,
                      aspectRatio: 2.0,
                      enlargeCenterPage: true,
                      enlargeStrategy: CenterPageEnlargeStrategy.height,
                    ),
                    items: Event.categories
                        .map((item) => HeroCarouselCard(
                              event: item,
                            ))
                        .toList(),
                  ),
                  const SizedBox(
                    height: 26,
                  ),
                  // Categories
                  SizedBox(
                    height: 150.0,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: Category.categories
                          .map((value) => CategoryCard(category: value))
                          .toList(),
                    ),
                  ),

                  // Repair Manual
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 14),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Padding(
                          padding: EdgeInsets.symmetric(horizontal: 16),
                          child: Expanded(
                            child: Text(
                              'Bài viết hướng dẫn sửa chữa',
                              style: TextStyle(
                                  fontSize: 17, fontWeight: FontWeight.w500),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        SizedBox(
                          height: 160,
                          child: ListView.separated(
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            scrollDirection: Axis.horizontal,
                            itemCount: RepairManual.repairManual.length,
                            itemBuilder: (BuildContext context, int index) {
                              return RepairManualCard(
                                repairManual: RepairManual.repairManual[index],
                              );
                            },
                            separatorBuilder: (_, __) =>
                                const SizedBox(width: 14),
                          ),
                        )
                      ],
                    ),
                  ),

                  // popular product
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 14),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Padding(
                          padding: EdgeInsets.symmetric(horizontal: 16),
                          child: Text(
                            'Sản phẩm phổ biến',
                            style: TextStyle(
                                fontSize: 17, fontWeight: FontWeight.w500),
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        SizedBox(
                          height: 300,
                          child: ListView.separated(
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            scrollDirection: Axis.horizontal,
                            itemCount: RepairManual.repairManual.length,
                            itemBuilder: (BuildContext context, int index) {
                              return ProductCard(
                                  product: Product.products[index]);
                            },
                            separatorBuilder: (_, __) =>
                                const SizedBox(width: 20),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      );
}
