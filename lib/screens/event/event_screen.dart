import 'package:flutter/material.dart';
import 'package:flutter_furniture/helpers/image_helper.dart';
import 'package:flutter_furniture/models/models.dart';
import 'package:flutter_furniture/widgets/widgets.dart';

class EventScreen extends StatelessWidget {
  const EventScreen({super.key, required this.event});

  static const String routeName = '/event';

  static Route route({required Event event}) {
    return MaterialPageRoute(
      settings: const RouteSettings(name: routeName),
      builder: (context) => EventScreen(event: event),
    );
  }

  final Event event;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppBar(screen: routeName),
      body: Padding(
        padding: const EdgeInsets.only(top: 20),
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: ImageHelper.loadFromAsset(
                event.imagePath,
                fit: BoxFit.cover,
                width: double.infinity,
                height: double.infinity,
              ),
            ),
            Expanded(
              flex: 2,
              child: Container(
                padding: const EdgeInsets.all(20),
                child: Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        const Icon(
                          Icons.timer,
                          color: Colors.black,
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Text(
                          'Từ ${event.startTime} đến ${event.endTime}',
                          style: TextStyle(
                              fontSize: 17, color: Colors.red.shade600),
                        ),
                      ],
                    ),
                    const SizedBox(height: 30),
                    Text(
                      event.contentEvent ??
                          'Hiện tại không có nội dung chi tiết',
                      style: const TextStyle(
                        fontSize: 17,
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
