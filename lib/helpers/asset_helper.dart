class AssetHelper {
  static const String coverImage = 'assets/images/coverImage.webp';
  // event images
  static const String event1 = 'assets/images/events/event.jpg';
  static const String event2 = 'assets/images/events/event2.jpg';
  static const String event3 = 'assets/images/events/event3.jpg';
  static const String event4 = 'assets/images/events/event4.jpg';
  static const String event5 = 'assets/images/events/event5.jpg';

  // category images
  static const String accessory = 'assets/images/categories/accessory.jpg';
  static const String bathroom = 'assets/images/categories/bathroom.jpg';
  static const String bedroom = 'assets/images/categories/bedroom.jpg';
  static const String kitchen = 'assets/images/categories/kitchen.jpg';
  static const String livingroom = 'assets/images/categories/livingroom.jpg';

  // repair manual
  static const String assemblingTheBed =
      'assets/images/repairManual/assembling_the_bed.jpg';
  static const String cleaningChair =
      'assets/images/repairManual/cleaning_chair.jpg';
  static const String kitchenCleaning =
      'assets/images/repairManual/kitchen_cleaning.jpg';
  static const String repairWashingMachines =
      'assets/images/repairManual/repair_washing_machines.jpg';

  // bathroom
  static const String bathroomSink1 =
      'assets/images/bathroom/sink/bathroom_sink1.png';
  static const String bathroomSink2 =
      'assets/images/bathroom/sink/bathroom_sink2.png';
  static const String bathroomToilet1 =
      'assets/images/bathroom/toilet/bathroom_toilet1.png';
  static const String bathroomToilet2 =
      'assets/images/bathroom/toilet/bathroom_toilet2.png';

  // bedroom
  static const String bedroomBed1 =
      'assets/images/bedroom/bed/bedroom_bed1.png';
  static const String bedroomBed2 =
      'assets/images/bedroom/bed/bedroom_bed2.png';

  static const String bedroomLamp1 =
      'assets/images/bedroom/lamp/bedroom_lamp1.png';
  static const String bedroomLamp2 =
      'assets/images/bedroom/lamp/bedroom_lamp2.png';

  static const String bedroomWardrobe1 =
      'assets/images/bedroom/wardrobe/bedroom_wardrobe1.png';
  static const String bedroomWardrobe2 =
      'assets/images/bedroom/wardrobe/bedroom_wardrobe2.png';

  // kitchen
  static const String kitchenFridge1 =
      'assets/images/kitchen/fridge/kitchen_fridge1.png';
  static const String kitchenFridge2 =
      'assets/images/kitchen/fridge/kitchen_fridge2.png';

  static const String kitchenGasStove1 =
      'assets/images/kitchen/gasStove/kitchen_gasStove1.png';
  static const String kitchenGasStove2 =
      'assets/images/kitchen/gasStove/kitchen_gasStove2.png';
  static const String kitchenGasStove3 =
      'assets/images/kitchen/gasStove/kitchen_gasStove3.png';

  static const String kitchenHood1 =
      'assets/images/kitchen/hood/kitchen_hood1.png';
  static const String kitchenHood2 =
      'assets/images/kitchen/hood/kitchen_hood2.png';

  static const String kitchenWashBasin1 =
      'assets/images/kitchen/washBasin/kitchen_washBasin1.png';

  // livingroom
  static const String livingroomChair1 =
      'assets/images/livingroom/chair/livingroom_chair1.png';
  static const String livingroomChair2 =
      'assets/images/livingroom/chair/livingroom_chair2.png';

  static const String livingroomTable1 =
      'assets/images/livingroom/table/livingroom_table1.png';
  static const String livingroomTable2 =
      'assets/images/livingroom/table/livingroom_table2.png';
}
