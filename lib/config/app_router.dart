import 'package:flutter/material.dart';
import 'package:flutter_furniture/models/models.dart';
import 'package:flutter_furniture/screens/screens.dart';

class AppRouter {
  static Route onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return HomeScreen.route();
      case '/splash_screen':
        return SplashScreen.route();
      case '/event':
        return EventScreen.route(event: settings.arguments as Event);
      case '/category':
        return CategoryScreen.route(category: settings.arguments as Category);
      case '/detail_product_screen':
        return DetailProductScreen.route(
            product: settings.arguments as Product);
      case '/search_detail_screen':
        return SearchResultScreen.route();
      case '/cart_screen':
        return CartScreen.route();
      case '/repair_manual_screen':
        return RepairManualScreen.route(
            repairManual: settings.arguments as RepairManual);
      default:
        return _errorRoute();
    }
  }

  static Route _errorRoute() {
    return MaterialPageRoute(
      settings: const RouteSettings(name: '/error'),
      builder: (_) => Scaffold(
        appBar: AppBar(
          title: const Text('Error'),
        ),
      ),
    );
  }
}
