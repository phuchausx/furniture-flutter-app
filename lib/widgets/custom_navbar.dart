import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_furniture/blocs/cart/cart_bloc.dart';
import 'package:flutter_furniture/models/models.dart';

class CustomNavBar extends StatelessWidget {
  const CustomNavBar({
    super.key,
    required this.screen,
    this.product,
  });

  final String screen;
  final Product? product;

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: -4,
              blurRadius: 12,
              offset: const Offset(-12, -12), // changes position of shadow
            ),
          ],
        ),
        height: 70,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: _selectNavBar(context, screen, product),
        ),
      ),
    );
  }

  Widget? _selectNavBar(context, screen, product) {
    switch (screen) {
      case '/detail_product_screen':
        return _buildDetailProduct(context, product);
      case '/cart_screen':
        return _buildCart(context);
      default:
        [];
    }
    return null;
  }

  Widget _buildDetailProduct(context, product) {
    return BlocBuilder<CartBloc, CartState>(
      builder: (context, state) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              margin: const EdgeInsets.only(right: 18),
              width: 58,
              height: 50,
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(18)),
                border: Border.all(color: Colors.blue.shade400),
              ),
              child: IconButton(
                icon: Icon(
                  Icons.add_shopping_cart,
                  color: Colors.blue.shade400,
                ),
                onPressed: () {
                  const snackBar =
                      SnackBar(content: Text('Sản phẩm đã thêm vào giỏ hàng'));
                  ScaffoldMessenger.of(context).showSnackBar(snackBar);

                  context.read<CartBloc>().add(AddProductToCartEvent(product));
                },
              ),
            ),
            Expanded(
              child: SizedBox(
                height: 50,
                child: ElevatedButton(
                  onPressed: () {
                    context
                        .read<CartBloc>()
                        .add(AddProductToCartEvent(product));

                    Navigator.pushNamed(context, '/cart_screen');
                  },
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.blue.shade400,
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(18),
                      ),
                    ),
                  ),
                  child: const Text(
                    'Mua ngay',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ),
            )
          ],
        );
      },
    );
  }

  Widget _buildCart(context) {
    return Align(
      alignment: Alignment.center,
      child: SizedBox(
        width: MediaQuery.of(context).size.width * 0.8,
        height: 50,
        child: ElevatedButton(
          onPressed: () {},
          style: ElevatedButton.styleFrom(
            backgroundColor: Colors.black,
            shape: const RoundedRectangleBorder(),
          ),
          child: const Text(
            'Thanh toán thôi nào ^-^',
            style: TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
      ),
    );
  }
}
