import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_furniture/blocs/cart/cart_bloc.dart';
import 'package:flutter_furniture/helpers/image_helper.dart';
import 'package:flutter_furniture/models/models.dart';
import 'package:flutter_furniture/widgets/price_product.dart';

class ProductCard extends StatelessWidget {
  const ProductCard({
    super.key,
    required this.product,
    this.widthFactor = 2.5,
  });

  final Product product;
  final double widthFactor;

  @override
  Widget build(BuildContext context) {
    final double widthValue = MediaQuery.of(context).size.width / widthFactor;
    return InkWell(
      onTap: () {
        Navigator.pushNamed(
          context,
          '/detail_product_screen',
          arguments: product,
        );
      },
      child: SizedBox(
        width: widthValue,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AspectRatio(
              aspectRatio: 1.02,
              child: Container(
                padding: const EdgeInsets.all(14),
                decoration: BoxDecoration(
                  color: Colors.grey.shade400,
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Hero(
                  tag: '${product.idProduct}',
                  child: ImageHelper.loadFromAsset(product.imageUrl),
                ),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Text(
              product.name,
              style: const TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  fontWeight: FontWeight.w500),
              maxLines: 2,
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                PriceProduct.loadProperty(
                  price: product.price,
                  discount: product.discount,
                ),
                BlocBuilder<CartBloc, CartState>(
                  builder: (context, state) {
                    if (state is CartLoading) {
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    }

                    if (state is CartLoaded) {
                      return Container(
                        width: 35,
                        height: 35,
                        decoration: BoxDecoration(
                          color: Colors.grey.shade400,
                          shape: BoxShape.circle,
                        ),
                        child: IconButton(
                          icon: const Icon(
                            Icons.add_shopping_cart,
                            color: Colors.black,
                            size: 20,
                          ),
                          onPressed: () {
                            const snackBar = SnackBar(
                                content: Text('Sản phẩm đã thêm vào giỏ hàng'));
                            ScaffoldMessenger.of(context)
                              ..removeCurrentSnackBar()
                              ..showSnackBar(snackBar);
                            context
                                .read<CartBloc>()
                                .add(AddProductToCartEvent(product));
                          },
                        ),
                      );
                    }

                    return const Text('Something went wrong');
                  },
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
