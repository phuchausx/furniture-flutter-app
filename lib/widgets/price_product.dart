import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class PriceProduct {
  static Widget loadProperty({
    String? discount,
    required String price,
    double? fontSizeDiscount,
    double? fontSizePrice,
  }) {
    final format = NumberFormat("###,###.###", "tr_TR");
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        discount != null
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '${format.format(double.parse(price))} VNĐ ',
                    style: TextStyle(
                      color: Colors.blue,
                      decoration: TextDecoration.lineThrough,
                      fontSize: fontSizePrice ?? 12,
                    ),
                  ),
                  Text(
                    '-${(double.parse(price) - double.parse(discount)) / double.parse(price) * 100}%',
                    style: TextStyle(
                        color: Colors.red, fontSize: fontSizePrice ?? 12),
                  ),
                ],
              )
            : const SizedBox(),
        Text(
          '${format.format(double.parse(discount ?? price))} VNĐ',
          style: TextStyle(
              fontSize:
                  (discount != null ? fontSizeDiscount : fontSizePrice) ?? 14,
              color: Colors.brown,
              fontWeight: FontWeight.w700),
        )
      ],
    );
  }
}
