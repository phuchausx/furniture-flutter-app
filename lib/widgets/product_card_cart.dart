import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_furniture/blocs/cart/cart_bloc.dart';
import 'package:flutter_furniture/helpers/image_helper.dart';
import 'package:flutter_furniture/models/models.dart';
import 'package:flutter_furniture/widgets/price_product.dart';

class ProductCardCart extends StatelessWidget {
  const ProductCardCart(
      {super.key, required this.product, required this.quantity});

  final Product product;
  final int quantity;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        SizedBox(
          width: 100,
          height: 100,
          child: Container(
            padding: const EdgeInsets.all(5),
            decoration: BoxDecoration(
              color: Colors.grey.shade400,
              borderRadius: const BorderRadius.all(Radius.circular(6)),
            ),
            child: ImageHelper.loadFromAsset(product.imageUrl),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(product.name),
                const SizedBox(height: 5),
                PriceProduct.loadProperty(
                    price: product.price, discount: product.discount),
              ],
            ),
          ),
        ),
        BlocBuilder<CartBloc, CartState>(
          builder: (context, state) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  children: [
                    IconButton(
                      icon: const Icon(
                        Icons.remove_circle,
                        color: Colors.black,
                      ),
                      onPressed: () {
                        context
                            .read<CartBloc>()
                            .add(RemoveProductFromCartEvent(product));
                      },
                    ),
                    Text(
                      quantity.toString(),
                      style: const TextStyle(color: Colors.black),
                    ),
                    IconButton(
                      icon: const Icon(
                        Icons.add_circle,
                        color: Colors.black,
                      ),
                      onPressed: () {
                        context
                            .read<CartBloc>()
                            .add(AddProductToCartEvent(product));
                      },
                    )
                  ],
                ),
                IconButton(
                  icon: const Icon(
                    Icons.delete_forever,
                    size: 30,
                  ),
                  color: Colors.black,
                  onPressed: () {
                    context
                        .read<CartBloc>()
                        .add(RemoveMultipleProductFromCartEvent(product));
                  },
                )
              ],
            );
          },
        ),
      ],
    );
  }
}
