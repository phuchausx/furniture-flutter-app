import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_furniture/blocs/productsByCategory/products_by_category_bloc.dart';
import 'package:flutter_furniture/helpers/image_helper.dart';
import 'package:flutter_furniture/models/models.dart';

class CategoryCard extends StatelessWidget {
  const CategoryCard({super.key, required this.category});

  final Category category;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProductsByCategoryBloc, ProductsByCategoryState>(
      builder: (context, state) {
        if (state is ProductsByCategoryLoading) {
          return const Center(child: CircularProgressIndicator());
        }

        if (state is ProductsByCategoryLoaded) {
          return InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/category', arguments: category);
              context
                  .read<ProductsByCategoryBloc>()
                  .add(ChangProducts(1, category.idCategory));
            },
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 16),
              constraints: const BoxConstraints(maxHeight: 73, maxWidth: 73),
              child: Column(
                children: [
                  ImageHelper.loadFromAsset(
                    category.imagePath,
                    width: 73,
                    height: 73,
                    radius: BorderRadius.circular(15),
                    fit: BoxFit.cover,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    category.nameCategory,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                        fontSize: 14, fontWeight: FontWeight.w500),
                  ),
                ],
              ),
            ),
          );
        } else {
          return const Text('Something went wrong');
        }
      },
    );
  }
}
