import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_furniture/blocs/cart/cart_bloc.dart';
import 'package:flutter_furniture/widgets/custom_search_delegate.dart';
import 'package:badges/badges.dart' as badges;

class CustomAppBar extends StatelessWidget with PreferredSizeWidget {
  const CustomAppBar({super.key, required this.screen});

  final String screen;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      child: _selectAppBar(context, screen),
    );
  }

  Widget? _selectAppBar(context, screen) {
    switch (screen) {
      case '/':
        return _buildHomeScreen(context, false);
      default:
        return _buildHomeScreen(context, true);
    }
  }

  Widget _buildHomeScreen(context, bool showButtonBack) {
    return AppBar(
      backgroundColor: Colors.transparent,
      automaticallyImplyLeading: showButtonBack ? true : false,
      leading: showButtonBack
          ? IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: const Icon(
                Icons.arrow_back_rounded,
                color: Colors.black,
              ),
            )
          : null,
      elevation: 0,
      title: TextField(
        readOnly: true,
        decoration: InputDecoration(
          hintText: 'Search Product',
          prefixIcon: const Icon(
            Icons.search,
            color: Colors.black,
          ),
          filled: true,
          fillColor: Colors.grey.shade200,
          border: const OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.all(Radius.circular(10)),
          ),
          contentPadding: const EdgeInsets.symmetric(horizontal: 10),
        ),
        onTap: () async {
          await showSearch<String>(
            context: context,
            delegate: CustomSearchDelegate(),
          );
        },
      ),
      titleSpacing: 15,
      actions: [
        Ink(
          decoration: ShapeDecoration(
            color: Colors.grey.shade200,
            shape: const CircleBorder(),
          ),
          child: IconButton(
            icon: BlocBuilder<CartBloc, CartState>(
              builder: (context, state) {
                if (state is CartLoaded) {
                  return badges.Badge(
                    showBadge: state.cart.products.length > 0 ? true : false,
                    badgeAnimation: const badges.BadgeAnimation.fade(),
                    badgeContent: Text(
                      '${state.cart.products.length}',
                      style: const TextStyle(color: Colors.white, fontSize: 11),
                    ),
                    child: const Icon(
                      Icons.shopping_cart,
                      color: Colors.black,
                    ),
                  );
                }
                return const badges.Badge(
                  badgeAnimation: badges.BadgeAnimation.fade(),
                  badgeContent: CircularProgressIndicator(),
                  child: Icon(
                    Icons.shopping_cart,
                    color: Colors.black,
                  ),
                );
              },
            ),
            onPressed: () {
              if (ModalRoute.of(context)?.settings.name == '/cart_screen') {
                return;
              } else {
                Navigator.pushNamed(context, '/cart_screen');
              }
            },
          ),
        ),
        const SizedBox(
          width: 10,
        ),
        Padding(
          padding: const EdgeInsets.only(right: 15),
          child: Ink(
            decoration: ShapeDecoration(
              color: Colors.grey.shade200,
              shape: const CircleBorder(),
            ),
            child: IconButton(
              icon: const Icon(
                Icons.notifications,
                color: Colors.black,
              ),
              onPressed: () {},
            ),
          ),
        )
      ],
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => const Size.fromHeight(50);
}
