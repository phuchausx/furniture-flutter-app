import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_furniture/blocs/productsByCategory/products_by_category_bloc.dart';
import 'package:flutter_furniture/models/models.dart';

class TabCategory extends StatefulWidget {
  const TabCategory({
    Key? key,
    required this.categoryDetail,
    required this.idCategory,
  }) : super(key: key);

  final int idCategory;
  final List<CategoryDetail> categoryDetail;

  @override
  _TabCategoryState createState() => _TabCategoryState();
}

class _TabCategoryState extends State<TabCategory> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 60,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: widget.categoryDetail.length,
        itemBuilder: (context, index) => buildCategory(index),
      ),
    );
  }

  Widget buildCategory(int index) {
    return BlocBuilder<ProductsByCategoryBloc, ProductsByCategoryState>(
      builder: (context, state) {
        return GestureDetector(
          onTap: () {
            setState(() {
              selectedIndex = index;
            });
            context.read<ProductsByCategoryBloc>().add(
                  ChangProducts(
                    widget.categoryDetail[index].id,
                    widget.idCategory,
                  ),
                );
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.categoryDetail[index].name,
                  style: TextStyle(
                    fontWeight: selectedIndex == index
                        ? FontWeight.w700
                        : FontWeight.w300,
                    color: Colors.black,
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(top: 10),
                  height: 2,
                  width: 30,
                  color: selectedIndex == index
                      ? Colors.black
                      : Colors.transparent,
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
