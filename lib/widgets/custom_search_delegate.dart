import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_furniture/blocs/listProductSearch/list_product_search_bloc.dart';
import 'package:flutter_furniture/models/models.dart';

class CustomSearchDelegate extends SearchDelegate<String> {
  @override
  Widget buildLeading(BuildContext context) => IconButton(
        icon: const Icon(Icons.chevron_left),
        onPressed: () => close(context, ''),
      );

  @override
  List<Widget> buildActions(BuildContext context) => [
        IconButton(
          icon: const Icon(Icons.clear),
          onPressed: () => query = '',
        )
      ];

  @override
  Widget buildResults(BuildContext context) => Container();

  @override
  void showResults(BuildContext context) {
    close(context, query);
    context
        .read<ListProductSearchBloc>()
        .add(ChangeListProductSearchEvent(query));
    Navigator.pushNamed(context, '/search_detail_screen');
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    List<Product> suggestions = Product.products.where((product) {
      final result = product.name.toLowerCase();
      final input = query.toLowerCase();

      return result.contains(input);
    }).toList();
    return BlocBuilder<ListProductSearchBloc, ListProductSearchState>(
      builder: (context, state) {
        return ListView.builder(
          itemCount: suggestions.length,
          itemBuilder: (context, index) {
            final suggestion = suggestions[index];

            return ListTile(
              title: Text(suggestion.name),
              onTap: () {
                query = suggestion.name;

                showResults(context);
                // close(context, query);
                // context
                //     .read<ListProductSearchBloc>()
                //     .add(ChangeListProductSearchEvent(query));
                // Navigator.pushNamed(context, '/search_detail_screen');
              },
            );
          },
        );
      },
    );
  }
}
