import 'package:flutter/material.dart';
import 'package:flutter_furniture/helpers/image_helper.dart';
import 'package:flutter_furniture/models/models.dart';

class RepairManualCard extends StatelessWidget {
  const RepairManualCard({
    super.key,
    required this.repairManual,
  });

  final RepairManual repairManual;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, '/repair_manual_screen',
            arguments: repairManual);
      },
      child: Stack(
        children: [
          ImageHelper.loadFromAsset(
            repairManual.imagePath,
            width: 300,
            height: 130,
            radius: BorderRadius.circular(20),
            fit: BoxFit.cover,
          ),
          Container(
            width: 300,
            height: 130,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: const Color.fromRGBO(0, 0, 0, 0.45),
            ),
          ),
          Container(
            color: Colors.red,
          ),
          SizedBox(
            width: 300,
            child: Padding(
              padding: const EdgeInsets.all(14),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    repairManual.titleRepairManual,
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    '${repairManual.numberView.toString()} lượt xem',
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 15,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
