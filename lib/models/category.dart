import 'package:equatable/equatable.dart';
import 'package:flutter_furniture/helpers/asset_helper.dart';

class Category extends Equatable {
  const Category({
    required this.idCategory,
    required this.nameCategory,
    required this.imagePath,
    required this.categoryDetails,
  });

  final int idCategory;
  final String nameCategory;
  final String imagePath;
  final List<CategoryDetail> categoryDetails;

  @override
  List<Object?> get props => [nameCategory, imagePath];

  static List<Category> categories = [
    const Category(
      idCategory: 1,
      nameCategory: 'Đồ nhà bếp',
      imagePath: AssetHelper.kitchen,
      categoryDetails: [
        CategoryDetail(id: 1, name: 'Máy hút mùi'),
        CategoryDetail(id: 2, name: 'Bếp gas'),
        CategoryDetail(id: 3, name: 'Tủ lạnh'),
        CategoryDetail(id: 4, name: 'Chậu rửa chén')
      ],
    ),
    const Category(
        idCategory: 2,
        nameCategory: 'Đồ phòng ngủ',
        imagePath: AssetHelper.bedroom,
        categoryDetails: [
          CategoryDetail(id: 1, name: 'Giường ngủ'),
          CategoryDetail(id: 2, name: 'Đèn ngủ'),
          CategoryDetail(id: 3, name: 'Tủ quần áo'),
        ]),
    const Category(
        idCategory: 3,
        nameCategory: 'Đồ phòng khách',
        imagePath: AssetHelper.livingroom,
        categoryDetails: [
          CategoryDetail(id: 1, name: 'Ghế sofa'),
          CategoryDetail(id: 2, name: 'Bàn phòng khách')
        ]),
    const Category(
        idCategory: 4,
        nameCategory: 'Đồ nhà tắm',
        imagePath: AssetHelper.bathroom,
        categoryDetails: [
          CategoryDetail(id: 1, name: 'Chậu rửa mặt'),
          CategoryDetail(id: 2, name: 'Bồn cầu'),
        ]),
    const Category(
      idCategory: 5,
      nameCategory: 'Phụ kiện',
      imagePath: AssetHelper.accessory,
      categoryDetails: [],
    ),
  ];
}

class CategoryDetail extends Equatable {
  const CategoryDetail({required this.id, required this.name});
  final int id;
  final String name;

  @override
  List<Object?> get props => [id, name];
}
