import 'package:equatable/equatable.dart';
import 'package:flutter_furniture/helpers/asset_helper.dart';

class Event extends Equatable {
  const Event({
    required this.nameEvent,
    required this.imagePath,
    this.startTime,
    this.endTime,
    this.contentEvent,
  });

  final String nameEvent;
  final String imagePath;
  final String? startTime;
  final String? endTime;
  final String? contentEvent;

  @override
  List<Object?> get props => [nameEvent, imagePath];

  static List<Event> categories = [
    const Event(
      nameEvent: 'Giảm giá các mặt hàng nội thất và nhà bếp',
      imagePath: AssetHelper.event1,
      startTime: '10/01/2023',
      endTime: '20/01/2023',
      contentEvent:
          'Trong thời gian tổ chức chương trình thì cửa hàng chúng tôi xin được giảm giá những mặt hàng sau:\n- Đối với các mặt hàng nội thất giảm 10%\n- Đối với mặt hàng nhà bếp được giảm 5%',
    ),
    const Event(
      nameEvent: 'Đi mua hàng nhưng có quà cầm về',
      imagePath: AssetHelper.event2,
      startTime: '15/03/2023',
      endTime: '30/03/2023',
      contentEvent:
          'Trong thời gian này khi quý khách ghé cửa hàng và mua sản phẩm thì có thể nhận được những phần quà vô cùng hấp dẫn',
    ),
    const Event(
      nameEvent: 'Những mặt hàng mới đã đến trong cửa hàng',
      imagePath: AssetHelper.event3,
      startTime: '11/04/2023',
      endTime: '30/05/2023',
      contentEvent:
          'Những mặt hàng mới đến được của hàng của chúng tôi. Quý khách có thể ghé tới cửa hàng để tham quan và mua sắm.\n Đặc biệt có những mặt hàng sẽ được giảm giá trong dịp này nhé',
    ),
    const Event(
      nameEvent: 'Tưng bừng giảm giá',
      imagePath: AssetHelper.event4,
      startTime: '22/05/2023',
      endTime: '26/05/2023',
      contentEvent:
          'Trong thời gian này cửa hàng chúng tôi xin gửi tới quý khách hàng những ưu đãi sau đây:\n- Đối với những quý khách hàng thành viên sẽ được giảm 5% trên tổng hoá đơn.\n- Đối với những khách hàng mua trên hoá đơn 5.000.000 thì sẽ nhận được phần quà vô cùng hấp dẫn.',
    ),
    const Event(
      nameEvent: 'Ở đây có chương trình giảm giá nè',
      imagePath: AssetHelper.event5,
      startTime: '03/08/2023',
      endTime: '20/08/2023',
      contentEvent: 'Không có giảm gì hết nhé',
    ),
  ];
}
