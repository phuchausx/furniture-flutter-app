import 'package:equatable/equatable.dart';
import 'package:flutter_furniture/helpers/asset_helper.dart';

class RepairManual extends Equatable {
  const RepairManual({
    required this.titleRepairManual,
    required this.numberView,
    required this.imagePath,
    required this.content,
    this.href,
  });

  final String titleRepairManual;
  final int numberView;
  final String imagePath;
  final String content;
  final String? href;

  @override
  static List<RepairManual> repairManual = [
    const RepairManual(
        titleRepairManual: 'Hướng dẫn lắp ráp giường',
        numberView: 2,
        imagePath: AssetHelper.assemblingTheBed,
        content:
            'Hướng dẫn lắp ráp giường là nội dung cần phải hiển thị lên cho người đọc tìm hiểu'),
    const RepairManual(
        titleRepairManual: 'Cách vệ sinh ghế sofa',
        numberView: 3,
        imagePath: AssetHelper.cleaningChair,
        content:
            'Cách vệ sinh ghế sofa là nội dung cần phải hiển thị lên cho người đọc tìm hiểu'),
    const RepairManual(
        titleRepairManual: 'Vệ sinh nhà bếp của bạn',
        numberView: 6,
        imagePath: AssetHelper.kitchenCleaning,
        content:
            'Vệ sinh nhà bếp của bạn là nội dung cần phải hiển thị lên cho người đọc tìm hiểu'),
    const RepairManual(
        titleRepairManual: 'Hướng dẫn sửa chữa máy giặt',
        numberView: 5,
        imagePath: AssetHelper.repairWashingMachines,
        content:
            'Hướng dẫn sửa chữa máy giặt là nội dung cần phải hiển thị lên cho người đọc tìm hiểu'),
  ];

  @override
  List<Object?> get props => [titleRepairManual, numberView, imagePath, href];
}
