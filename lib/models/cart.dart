import 'package:equatable/equatable.dart';
import 'package:flutter_furniture/models/models.dart';
import 'package:intl/intl.dart';

class Cart extends Equatable {
  const Cart({this.products = const <Product>[]});

  final List<Product> products;

  static const int deliveryFeeRate = 500000;
  static const int deliveryFeeValue = 30000;
  static var format = NumberFormat("###,###.###", "tr_TR");

  int get totalMoneyOfProduct => products.fold(0,
      (total, current) => total + int.parse(current.discount ?? current.price));

  int deliveryFee(total) {
    if (total >= deliveryFeeRate) {
      return 0;
    }
    return deliveryFeeValue;
  }

  int totalBill(totalMoneyOfProduct, deliveryFee) {
    return totalMoneyOfProduct + deliveryFee;
  }

  String deliveryFeeNotification(totalMoneyOfProduct) {
    if (totalMoneyOfProduct >= deliveryFeeRate) {
      return 'Bạn được miễn phí giao hàng';
    }
    return 'Bạn cần phải mua thêm ${format.format(deliveryFeeRate - totalMoneyOfProduct)} VNĐ để được miễn phí giao hàng';
  }

  String get totalMoneyOfProductString => format.format(totalMoneyOfProduct);
  String get deliveryFeeString =>
      format.format(deliveryFee(totalMoneyOfProduct));
  String get totalBillString => format
      .format(totalBill(totalMoneyOfProduct, deliveryFee(totalMoneyOfProduct)));
  String get deliveryFeeNotificationString =>
      deliveryFeeNotification(totalMoneyOfProduct);

  Map productQuantity(List<Product> products) {
    var quantity = Map();

    products.forEach((value) {
      if (quantity.containsKey(value)) {
        quantity[value] += 1;
      } else {
        quantity[value] = 1;
      }
    });

    return quantity;
  }

  @override
  List<Object?> get props => [products];
}
