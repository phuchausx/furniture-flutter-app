import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_furniture/blocs/cart/cart_bloc.dart';
import 'package:flutter_furniture/blocs/listProductSearch/list_product_search_bloc.dart';
import 'package:flutter_furniture/blocs/productsByCategory/products_by_category_bloc.dart';
import 'package:flutter_furniture/config/app_router.dart';
import 'package:flutter_furniture/config/theme.dart';
import 'package:flutter_furniture/screens/screens.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Bloc.observer = SimpleBlocObserver();
  runApp(MyApp());
}

class SimpleBlocObserver extends BlocObserver {
  @override
  void onChange(BlocBase bloc, Change change) {
    super.onChange(bloc, change);
    print('${bloc.runtimeType} $change');
  }
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
            create: (_) => ProductsByCategoryBloc()..add(LoadProducts())),
        BlocProvider(
            create: (_) =>
                ListProductSearchBloc()..add(LoadListProductSearchEvent())),
        BlocProvider(create: (_) => CartBloc()..add(LoadCartEvent())),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: theme(),
        onGenerateRoute: AppRouter.onGenerateRoute,
        initialRoute: SplashScreen.routeName,
      ),
    );
  }
}
