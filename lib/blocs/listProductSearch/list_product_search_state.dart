part of 'list_product_search_bloc.dart';

abstract class ListProductSearchState extends Equatable {
  const ListProductSearchState();

  @override
  List<Object> get props => [];
}

class ListProductSearchLoading extends ListProductSearchState {
  @override
  List<Object> get props => [];
}

class ListProductSearchLoaded extends ListProductSearchState {
  final List<Product> products;

  ListProductSearchLoaded({this.products = const <Product>[]});

  @override
  List<Object> get props => [products];
}

class ListProductSearchError extends ListProductSearchState {
  @override
  List<Object> get props => [];
}
