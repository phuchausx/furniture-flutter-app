part of 'list_product_search_bloc.dart';

abstract class ListProductSearchEvent extends Equatable {
  const ListProductSearchEvent();

  @override
  List<Object> get props => [];
}

class LoadListProductSearchEvent extends ListProductSearchEvent {}

class ChangeListProductSearchEvent extends ListProductSearchEvent {
  final String resultNameSearch;

  ChangeListProductSearchEvent(this.resultNameSearch);

  @override
  List<Object> get props => [resultNameSearch];
}
