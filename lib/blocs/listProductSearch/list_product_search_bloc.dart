import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_furniture/models/models.dart';

part 'list_product_search_event.dart';
part 'list_product_search_state.dart';

class ListProductSearchBloc
    extends Bloc<ListProductSearchEvent, ListProductSearchState> {
  ListProductSearchBloc() : super(ListProductSearchLoading()) {
    on<LoadListProductSearchEvent>(_onLoadListProductSearchEvent);

    on<ChangeListProductSearchEvent>(_onChangeListProductSearchEvent);
  }

  void _onLoadListProductSearchEvent(
      event, Emitter<ListProductSearchState> emit) async {
    emit(ListProductSearchLoading());

    try {
      await Future<void>.delayed(const Duration(seconds: 1));
      emit(ListProductSearchLoaded());
    } catch (_) {
      emit(ListProductSearchError());
    }
  }

  void _onChangeListProductSearchEvent(
      event, Emitter<ListProductSearchState> emit) {
    final state = this.state;
    if (state is ListProductSearchLoaded) {
      try {
        emit(ListProductSearchLoaded(
            products: Product.products
                .where((value) => value.name
                    .toLowerCase()
                    .contains(event.resultNameSearch.toString().toLowerCase()))
                .toList()));
      } on Exception {
        emit(ListProductSearchError());
      }
    }
  }
}
