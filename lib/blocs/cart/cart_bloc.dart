import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_furniture/models/models.dart';

part 'cart_event.dart';
part 'cart_state.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  CartBloc() : super(CartLoading()) {
    on<LoadCartEvent>(_onLoadCartEvent);
    on<AddProductToCartEvent>(_onAddProductToCartEvent);
    on<RemoveProductFromCartEvent>(_onRemoveProductFromCartEvent);
    on<RemoveMultipleProductFromCartEvent>(
        _onRemoveMultipleProductFromCartEvent);
  }

  void _onLoadCartEvent(event, Emitter<CartState> emit) async {
    emit(CartLoading());

    try {
      await Future<void>.delayed(const Duration(seconds: 1));
      emit(CartLoaded());
    } catch (_) {
      emit(CartError());
    }
  }

  void _onAddProductToCartEvent(event, Emitter<CartState> emit) {
    final state = this.state;
    if (state is CartLoaded) {
      try {
        emit(
          CartLoaded(
              cart: Cart(
            products: List.from(state.cart.products)..add(event.product),
          )),
        );
      } on Exception {
        emit(CartError());
      }
    }
  }

  void _onRemoveProductFromCartEvent(event, Emitter<CartState> emit) {
    final state = this.state;
    if (state is CartLoaded) {
      try {
        if (state.cart.products
                .where((value) => value.idProduct == event.product.idProduct)
                .toList()
                .length >
            1) {
          emit(CartLoaded(
              cart: Cart(
            products: List.from(state.cart.products)..remove(event.product),
          )));
        }
      } on Exception {
        emit(CartError());
      }
    }
  }

  void _onRemoveMultipleProductFromCartEvent(event, Emitter<CartState> emit) {
    final state = this.state;
    if (state is CartLoaded) {
      try {
        emit(
          CartLoaded(
              cart: Cart(
            products: List.from(state.cart.products)
              ..removeWhere(
                  (value) => value.idProduct == event.product.idProduct),
          )),
        );
      } on Exception {
        emit(CartError());
      }
    }
  }
}
