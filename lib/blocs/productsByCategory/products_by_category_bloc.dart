import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_furniture/models/models.dart';

part 'products_by_category_event.dart';
part 'products_by_category_state.dart';

class ProductsByCategoryBloc
    extends Bloc<ProductsByCategoryEvent, ProductsByCategoryState> {
  ProductsByCategoryBloc() : super(ProductsByCategoryLoading()) {
    on<LoadProducts>(_onLoadProducts);
    on<ChangProducts>(_onChangeProducts);
  }

  void _onLoadProducts(event, Emitter<ProductsByCategoryState> emit) async {
    emit(ProductsByCategoryLoading());

    try {
      await Future<void>.delayed(const Duration(seconds: 1));
      emit(ProductsByCategoryLoaded());
    } catch (_) {
      emit(ProductsByCategoryError());
    }
  }

  void _onChangeProducts(event, Emitter<ProductsByCategoryState> emit) {
    final state = this.state;
    if (state is ProductsByCategoryLoaded) {
      try {
        emit(ProductsByCategoryLoaded(
          products: Product.products
              .where((value) =>
                  (value.idCategoryDetail == event.idCategoryDetail) &&
                  (value.idCategory == event.idCategory))
              .toList(),
        ));
      } on Exception {
        emit(ProductsByCategoryError());
      }
    }
  }
}
