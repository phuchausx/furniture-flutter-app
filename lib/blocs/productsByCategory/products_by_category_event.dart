part of 'products_by_category_bloc.dart';

abstract class ProductsByCategoryEvent extends Equatable {
  const ProductsByCategoryEvent();

  @override
  List<Object> get props => [];
}

class LoadProducts extends ProductsByCategoryEvent {}

class ChangProducts extends ProductsByCategoryEvent {
  final int idCategoryDetail;
  final int idCategory;

  ChangProducts(this.idCategoryDetail, this.idCategory);

  @override
  List<Object> get props => [idCategoryDetail];
}
