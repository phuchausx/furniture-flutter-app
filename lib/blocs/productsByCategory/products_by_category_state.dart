part of 'products_by_category_bloc.dart';

abstract class ProductsByCategoryState extends Equatable {
  const ProductsByCategoryState();

  @override
  List<Object> get props => [];
}

class ProductsByCategoryLoading extends ProductsByCategoryState {
  @override
  List<Object> get props => [];
}

class ProductsByCategoryLoaded extends ProductsByCategoryState {
  final List<Product> products;

  ProductsByCategoryLoaded({this.products = const <Product>[]});

  @override
  List<Object> get props => [products];
}

class ProductsByCategoryError extends ProductsByCategoryState {
  @override
  List<Object> get props => [];
}
